from flask import Flask
from flask import render_template

app = Flask(__name__)

@app.route('/')
def inicio():
    return "<h2>Para mostrar el diccionario vaya a la ventana / ven1 con Flask</h2>"

@app.route('/ven1')
def slang():
    palabras = { 'Xopa': 'que paso',
                 'Mopri':'Amigo o pasiero',
                 'Chombo':'Moreno o negro',
                 'Pelaita':'Niña de corta edad',
                 'Chiquillo':'Niño Pequeño',
                 'Man':'Hombre'}
    return "<h2>Las palabras del argot significan:  {}</h2>".format(palabras)

if __name__ == "__main__":
    app.run(host='127.0.0.1', port=8000, debug=True)

